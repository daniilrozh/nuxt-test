import JsonDB from 'node-json-db'
import { Config } from 'node-json-db/dist/lib/JsonDBConfig'

// The second argument is used to tell the DB to save after each push
// If you put false, you'll have to call the save() method.
// The third argument is to ask JsonDB to save the database in an human readable format. (default false)
// The last argument is the separator. By default it's slash (/)
const db = new JsonDB(new Config("database", true, true, '/'))

const {Router} = require('express')
const router = Router()

router.get('/api/:catalog', (req, res) => {
  try {
    res.json(db.getData(`/data/${req.params.catalog}`))
  } catch (error) {
    console.error(error)
  }
})

router.get('/api/test/:name', (req, res) => {
  db.push("/data/test", {name: req.params.name});
  res.json(db.getData('/data/test'))
})

module.exports = router
